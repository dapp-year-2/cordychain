const mongoose = require('mongoose')

const bafraSchema = new mongoose.Schema({
    bafraId: {
        type: String,
        require: [true, 'please enter bafra id'],

    },
})

const Bafra = mongoose.model('Bafra',bafraSchema)
module.exports=Bafra


