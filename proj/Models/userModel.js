// const mongoose = require('mongoose')
// const validator = require('validator')
// const bcrypt = require('bcryptjs')

// const userSchema = new mongoose.Schema({
//     name: {
//         type: String,
//         require: [true, 'please provide your name'],

//     },
//     eID: {
//         type: String,
//         require: [true, 'please provide your employeeID'],

//     },
//     email: {
//         type: String,
//         require: [true, 'Please enter your email'],
//         unique:true,
//         lowercase:true,
//         validator: [validator.isEmail, 'please provide a valid email'],

//     },
  
//     password:{
//         type:String,
//         require: [true, 'please enter a password'],

//     },
//     verified:{
//         type:Boolean,
//         default: false,
//         select:true,
//     },
//     active:{
//         type:Boolean,
//         default: true,
//         select:false,
//     },
//     passwordConfirm:{
//         type:String,
//         required: [true, 'please confirm your password'],
//         validate:{
//             validator: function (el){
//                 return el === this.password
//             },
//             message: 'Passwords are not the same',
//         },
//     },
    

// })
// userSchema.pre('save', async function (next) {
//     if (!this.isModified('password'))return next()
//     this.password = await bcrypt.hash(this.password,12)
//     this.passwordConfirm=undefined
//     next()
// })

// userSchema.methods.correctPassword = async function(
//     candidatePassword,
//     userPassword,
// ){
//     return await bcrypt.compare(candidatePassword,userPassword)
// }



// const User = mongoose.model('User',userSchema)
// module.exports=User

const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');


const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Please provide your name'],
  },
  eID: {
    type: String,
    required: [true, 'Please provide your employee ID'],
  },
  email: {
    type: String,
    required: [true, 'Please enter your email'],
    unique: true,
    lowercase: true,
    validate: [validator.isEmail, 'Please provide a valid email'],
  },
  password: {
    type: String,
    required: [true, 'Please enter a password'],
  },
  verified: {
    type: Boolean,
    default: false,
    select: true,
  },
  active: {
    type: Boolean,
    default: true,
    select: false,
  },
  passwordConfirm: {
    type: String,
    required: [true, 'Please confirm your password'],
    validate: {
      validator: function (el) {
        return el === this.password;
      },
      message: 'Passwords are not the same',
    },
  },
  otp: {
    type: String,
  },

});

userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();
  this.password = await bcrypt.hash(this.password, 12);
  this.passwordConfirm = undefined;
  next();
});

userSchema.methods.correctPassword = async function (
  candidatePassword,
  userPassword,
) {
  return await bcrypt.compare(candidatePassword, userPassword);
};


const User = mongoose.model('User', userSchema);
module.exports = User;

