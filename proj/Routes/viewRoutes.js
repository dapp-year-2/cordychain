const express = require('express')
const router=express.Router()
const viewController=require('.././Controllers/viewController')
// const authController=require('.././controllers/authController')
router.get('/',viewController.getHome)
router.get('/about',viewController.getAbout)
router.get('/login',viewController.getLoginForm)

router.get('/signup',viewController.getSignupForm)
module.exports=router