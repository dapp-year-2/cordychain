const express = require('express')
const userControllers = require('./../Controllers/userController')
const authcontrollers=require('./../Controllers/authController')
const router = express.Router()

router.post('/Signup', authcontrollers.signup)
router.post('/login', authcontrollers.login)
// .post('/:id/sendOTP', userControllers.sendOTP)


router
    .route('/')
    .get(userControllers.getAllUser)
    .post(userControllers.createUser)
   

router
    .route('/:id')
    // .post('/sendOTP', authcontrollers.protect, userControllers.sendOTP)
    .get(userControllers.getUser)
    .patch(userControllers.updateUser)
    .delete(userControllers.deleteUser)

module.exports=router
