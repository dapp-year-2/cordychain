const User = require('./../Models/userModel')
// const nodemailer = require('nodemailer')
// const Email = require('emailjs-com');
const otpGenerator = require("otp-generator");


exports.getAllUser = async (req, res, next) => {
    try{
        const users = await User.find()
        res.status(200).json({data: users, status: 'success'})
    } catch(err){
        res.status(500).json({error: err.message});
    }
}


exports.createUser = async (req, res, next) => {
    try {
        
        const otp = otpGenerator.generate(6, { upperCase: false, specialChars: false });

        
        
        const user = new User({
            name: req.body.name,
            email: req.body.email,
            eID:req.body.eID,
            password: req.body.password,
            passwordConfirm: req.body.passwordConfirm,
            otp:otp // add the generated OTP to the user object
        });

        await user.save();

        // const transporter = nodemailer.createTransport({
        //     host: "smtp.gmail.com",
        //     port: 587,
        //     secure: false,
        //     auth: {
        //         user: 'purugcit79@gmail.com', 
                
        //         pass: '12210079G' 
        //     }
        // });
        // transporter.sendMail({
        //     from: "purugcit79@gmail.com",
        //     to: user.email,
        //     subject: "Your OTP",
        //     text: `Your OTP is ${otp}.`,
        //     html: `<p>Your OTP is <strong>${otp}</strong>.</p>`
        // })
        // .then(info => console.log(info))
        // .catch(error => console.log(error));

        res.json({ data: user, status: 'success' })
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

// exports.createUser = async (req, res, next) => {
//     try {
//       const otp = otpGenerator.generate(6, { upperCase: false, specialChars: false });
  
//       const user = new User({
//         name: req.body.name,
//         email: req.body.email,
//         eID: req.body.eID,
//         password: req.body.password,
//         passwordConfirm: req.body.passwordConfirm,
//         otp: otp // add the generated OTP to the user object
//       });
  
//       await user.save();
  
//       const emailBody = `Your OTP is ${otp}.`;
  
//       Email.send({
//         Host: "smtp.elasticemail.com",
//         Username: "apipuru",
//         // Password: "5D8CC9E9967FD168C53DB5170B411693B11E486E1791298D2230862DCDD9D29B4671A76E8D8ABF66135917A8B65D899A",
//         password:"C87E49251F01652E654F12ACCDF1524E57B32C9BFADBC060B9C6E2C49D4E806147E3C4839D88DF64B586153EC74F7044",
//         To: user.email,
//         // From: "you@isp.com",
//         From:"purugcit79@gmail.com",
//         Subject: "Your OTP",
//         Body: emailBody
//       }).then(message => console.log(message));
  
//       res.json({ data: user, status: 'success' });
//     } catch (err) {
//       res.status(500).json({ error: err.message });
//     }
//   };
exports.getUser = async (req, res,) => {
    try{
        const user = await User.findById(req.params.id);
        
        res.json({data:user, status: 'success'})
    } catch(err){
        res.status(500).json({error: err.message});
    }
}

exports.updateUser = async (req, res,) => {
    try{
        const user = await User.findByIdAndUpdate(req.params.id,req.body);
        
        res.json({data:user, status: 'success'})
    } catch(err){
        res.status(500).json({error: err.message});
    }
}

exports.deleteUser = async (req, res,) => {
    try{
        const user = await User.findByIdAndDelete(req.params.id);
        
        res.json({data:user, status: 'success'})
    } catch(err){
        res.status(500).json({error: err.message});
    }
}

