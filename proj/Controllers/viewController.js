
const path = require ('path')

exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../','Views','SignIn.html'))
}
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname,'../','Views','index.html'))
}
exports.getAbout = (req, res) => {
    res.sendFile(path.join(__dirname,'../','Views','about.html'))
}

exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../','Views','signup.html'))
}